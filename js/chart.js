const ctx = document.getElementById('myChart').getContext('2d');

const data = {
    labels: [
        'Переводы внутри системы',
        'Переводы в другие системы',
        'Обмен валют внутри системы',
        'Кредитные операции',
        'Операции с картами RS Mastercard'
    ],
    max: 0,
    datasets: [{
        label: 'Dataset 1',
        data: [36, 27, 14, 12, 11],
        backgroundColor: ['#F9CA83', '#FE837A', '#8ADCAB', '#C492C6', '#65B9D0'],
    }]
};

const config = {
    type: 'pie',
    data: data,
    options: {
        responsive: true,
        plugins: {
            legend: {
                display: false
            },
        }
    },
};

const myChart = new Chart(ctx, config);