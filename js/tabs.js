function tabs() {
    const triggers = document.querySelectorAll(".sidebar_field");
    const tabs = document.querySelectorAll(".art");
    let selectedTabIndex = 0;

    setListeners();
    hideAllTabs();
    showTab(selectedTabIndex);

    function hideAllTabs() {
        tabs.forEach(tab => {
            tab.classList.add('art--hide')
        })
    }

    function showTab(index) {
        tabs[index].classList.remove('art--hide')
    }

    function unactiveAllTriggers() {
        triggers.forEach(trigger => {
            trigger.classList.remove('sidebar_field--active');
        })
    }

    function activateTriggerByIndex(index) {
        triggers[index].classList.add('sidebar_field--active');
    }

    function setListeners() {
        triggers.forEach((trigger, index) => {
            trigger.addEventListener('click', () => {
                selectedTabIndex = index;
                hideAllTabs();
                showTab(selectedTabIndex + 1);
                unactiveAllTriggers();
                activateTriggerByIndex(selectedTabIndex);
            })
        })
    }
}

tabs();