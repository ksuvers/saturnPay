var swiper = new Swiper(".slider", {
    slidesPerView: 3.4,
    autoHeight: true,
    loop: true,

    centeredSlides: true,

    navigation: {
        nextEl: '.btn_next',
        prevEl: '.btn_prev',
    },

    breakpoints: {
        315: {
            grid: {
                rows: 2,
                fill: 'row'
            },
            loop: false,
            autoHeight: false,
            centeredSlides: false,

            // slidesPerView: 2,
            slidesPerView: 1,
            // slidesPerColumn: 2,
            pagination: {
                el: ".swiper_pag",
                clickable: true,
            },

        },
        720: {
            slidesPerView: 2,
            spaceBetween: 10,
            centeredSlides: false,
            pagination: false
        },
        1100: {
            slidesPerView: 2.7,
            centeredSlides: true
        },
        1350: {
            slidesPerView: 3.4,
            spaceBetween: 23,
        },
        1500: {
            slidesPerView: 3.7
        },
        1750: {
            slidesPerView: 4.4
        }
    },
});
$(function() {
    $('.bg_menu').on('click', function() {
        $('.bg_list').slideToggle();
        this.classList.toggle('rotate')
    });
});


AOS.init({
    duration: 2000,
});